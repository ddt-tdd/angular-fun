import { Component } from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  defaultSubscription = 'Advanced';
  email = '';
  subscription = '';
  password = '';
  submitted = false;

  onSubmit(data: NgForm) {
    console.log(data.value);
    this.email = data.value.email;
    this.password = data.value.password;
    this.subscription = data.value.subscription;

    data.form.reset({
      subscription: this.defaultSubscription,
    });

    this.submitted = true;
  }
}
