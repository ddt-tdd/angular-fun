import { Component } from '@angular/core';

@Component({
  selector: 'app-warning-alert',
  template: '<H1>WARNING</H1>',
  styles: ['H1 {color: green}']
})
export class WarningAlertComponent {
}

