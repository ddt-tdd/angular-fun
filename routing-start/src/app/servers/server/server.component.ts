import { Component, OnInit } from '@angular/core';

import { ServersService } from '../servers.service';
import {ActivatedRoute, Data, Params, Router} from '@angular/router';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
  server: {id: number, name: string, status: string};

  constructor(private serversService: ServersService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    // const id = this.route.snapshot.params['id'];
    // this.server = this.serversService.getServer(parseInt(id, 10));
    // this.route.params.subscribe(
    //   (params: Params) => {
    //     this.server = this.serversService.getServer(parseInt(params['id'], 10));
    //   });
    this.route.data.subscribe(
      (data: Data) => {
        this.server = data['server']; // Must match to the name of routing table
      }
    );
  }

  onEdit() {
    this.router.navigate(['edit'], {relativeTo: this.route, queryParamsHandling: 'preserve'});
  }
}
