import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-display-details',
  templateUrl: './display-details.component.html',
  styleUrls: ['./display-details.component.css']
})
export class DisplayDetailsComponent {
  visible = false;
  timestamps = [];
  timestamp: number = null;

  onClick() {
    this.visible = !this.visible;
    const date = Date.now();
    this.timestamps.push(date);
    if (this.timestamps.length === 5) {
      this.timestamp = date;
    }
  }

  isAfter5th(ts: number): boolean {
    if (this.timestamp) {
      console.log((new Date(ts)).toTimeString() + ' vs ' + (new Date(this.timestamp)).toTimeString());
    }
    return this.timestamp && (ts > this.timestamp);
  }

  getColor(ts: number): string {
    if (this.isAfter5th(ts)) {
      return 'blue';
    } else {
      return '';
    }
  }

  getTimeString(ts: number): string {
    return (new Date(ts)).toTimeString();
  }

  isVisible(): boolean {
    return this.visible;
  }
}
