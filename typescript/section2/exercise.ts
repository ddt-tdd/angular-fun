type BankAccount = { money: number, deposit: (val: number) => void };

let bankAccount: BankAccount = {
    money: 2000,
    deposit: function(value: number): void {
        this.money += value;
    }
};

type Myself = {
    name: string,
    bankAccount: BankAccount,
    hobbies: string[]
}
let myself: Myself = {
    name: "Max",
    bankAccount: bankAccount,
    hobbies: ["Sports", "Cooking"]
};

myself.bankAccount.deposit(3000);

console.log(myself);
