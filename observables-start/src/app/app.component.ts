import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from './user.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  subscription : Subscription;
  userAcitvated = false;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.subscription = this.userService.activatedEmitter.subscribe( didActivated => {
      this.userAcitvated = didActivated;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
