import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  projectStatusForm : FormGroup;

  projectNames = ['ReactiveProject', 'TemplateProject'];

  ngOnInit(): void {
    this.projectStatusForm = new FormGroup({
      'projectname': new FormControl(null, [Validators.required, this.forbiddenProjectName], this.invalidProjectName.bind(this)),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'status': new FormControl('critical')
    });
  }


  onSubmit() {
    console.log(this.projectStatusForm);
  }

  forbiddenProjectName(value: FormControl): {[s: string]: boolean} {
    console.log('forbidden: ');
    console.log(value);

    if (value.value === 'Test') {
      return { 'forbiddenProjectName': true};
    }

    return null;
  }

  invalidProjectName(control: FormControl): Promise<any> | Observable<any> {
    console.log('taken: ');
    console.log(control);

    const promise = new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if (this.projectNames.indexOf(control.value) !== -1) {
          resolve({ 'projectNameTaken': true});
        } else {
          resolve(null);
        }
      }, 2000);
    });

    return promise;
  }
}
