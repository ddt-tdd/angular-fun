import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import CreateRequest = admin.auth.CreateRequest;
import UserRecord = admin.auth.UserRecord;

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

export const addMessage = functions.https.onRequest((req, resp) => {
    const app = admin.initializeApp();
    const userRequest : {
        email: string;
        password: string;
        returnSecureToken: boolean;
    } = JSON.parse(req.body);
    const user : CreateRequest = {
      email: userRequest.email,
      emailVerified: true,
      displayName: userRequest.email,
      password: userRequest.password
    };

    app.auth().createUser(user)
        .then((userRecord: UserRecord) => {
admin.auth().createCustomToken()
        })
});
