import {HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";

export class LoggingInterceptorService implements HttpInterceptor{
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('Outgoing' : req.url),
    return next.handle(req).pipe(tap(event => {
      console.log(event.type);
      if (event.type === HttpEventType.User) {
        console.log('User Event');
      }
      if (event.type === HttpEventType.Sent) {
        console.log('Message is sent');
      }
      if (event.type === HttpEventType.UploadProgress) {
        console.log('Upload in progress');
      }
      if (event.type === HttpEventType.DownloadProgress) {
        console.log('Download in progress');
      }
      if (event.type === HttpEventType.ResponseHeader) {
        console.log('RepsonseHeader arrived progress');
      }
      if (event.type === HttpEventType.Response) {
        console.log('Response arrived');
      }
    }));
  }
}
