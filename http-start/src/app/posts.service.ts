import {Injectable} from "@angular/core";
import {HttpClient, HttpEventType, HttpHeaders, HttpParams} from "@angular/common/http";
import {PostModel} from "./post.model";
import {catchError, map, tap} from "rxjs/operators";
import {Subject, throwError} from "rxjs";

@Injectable({providedIn: 'root'})
export class PostsService {
  error = new Subject<string>();

  constructor(private httpClient: HttpClient) {}

  createAnsStorePost(title: string, content: string) {
    const postData: PostModel = {title: title, content: content};

    // Send Http request
    this.httpClient.post<{name: string}>(
      'https://ng-http-01.firebaseio.com/posts.json',
      postData,
      {
        observe: 'response',
        responseType: 'text'
      }
    ).subscribe( responseData => {
      console.log(responseData);
    },
    error => {
      this.error.next(error.message);
    });
  }

  fetchPosts() {

    return this.httpClient.get<{ [key: string]: PostModel }>('https://ng-http-01.firebaseio.com/posts.json',
      {
        headers: new HttpHeaders({'Custom-header': 'hello'}),
        params: new HttpParams().append('print', 'pretty').append('custom', 'key')
      })
      .pipe(map(responseData  => {
        const postArray: PostModel[] = [];
        for (const key in responseData) {
          if (responseData.hasOwnProperty(key)) {
            postArray.push({ ...responseData[key], id: key});
          }
        }
        return postArray;
      }),
      catchError(errorRes => {
        // send to analytics server
        // pass on to throwError which is an observable
        return throwError(errorRes);
      }));
  }

  deletePosts() {
    // Send Http request
    return this.httpClient.delete<{name: string}>(
      'https://ng-http-01.firebaseio.com/posts.json',
      {
        observe: 'events'
      }
    ).pipe(tap(event => {
      console.log(event);
      if (event.type === HttpEventType.Sent) {
        console.log('messaged sent');
      }
      if (event.type === HttpEventType.Response) {
        console.log(event.body);
      }
    }));
  }
}
