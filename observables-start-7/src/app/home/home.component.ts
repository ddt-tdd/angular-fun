import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval, Observable, Observer, Subscription} from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  numberObsSubscription: Subscription;
  myObsSubscription: Subscription;

  ngOnDestroy(): void {
    this.numberObsSubscription.unsubscribe();
    this.myObsSubscription.unsubscribe();
  }

  constructor() { }

  ngOnInit() {
    const myNumber = interval(1000).pipe(map((data: number) => data * 2 ));

    this.numberObsSubscription = myNumber.subscribe((number: number) => {
      console.log(number);
    });

    const myObservable = Observable.create((observer: Observer<string>) => {
      setTimeout(() => {
        observer.next('First data package');
      }, 2000);
      setTimeout(() => {
        observer.next('Second data package');
      }, 4000);
      setTimeout(() => {
        observer.error('Error from observer');
  //      observer.complete();
      }, 5000);
      setTimeout(() => {
        observer.next('Third data package');
      }, 6000);
    });

   this.myObsSubscription = myObservable.subscribe(
      (data: string) => {
        console.log(data);
      },
      (err: string) => {
        console.log(err);
      },
      () => {
          console.log('completed');
      }
      );
  }
}
