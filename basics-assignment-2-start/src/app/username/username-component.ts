import { Component } from '@angular/core';

@Component({
  selector: 'app-username',
  templateUrl: './username-component.html'
  })
export class UsernameComponent {
  username = '';

  /**
   * Check if username is any empty string
   */
  isEmpty(): boolean {
    return this.username === '';
  }

  /**
   * reset the username to empty string
   */
  reset() {
    this.username = '';
  }
}
