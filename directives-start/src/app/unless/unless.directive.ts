import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {

  // This is a setter
  // Must be the same name to be called from the ng-template
  @Input() set appUnless(condition: boolean) {
    if (!condition) {
      this.vcRef.createEmbeddedView(this.templateRef);
    } else {
      this.vcRef.clear();
    }

  }

  // TemplateRef reference to the template ng-template
  // ViewContainerRef reference where the template must be placed
  constructor(private templateRef: TemplateRef<any>, private vcRef: ViewContainerRef) {

  }

}
