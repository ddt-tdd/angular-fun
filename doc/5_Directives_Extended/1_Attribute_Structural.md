Attribute vs Structural Directives
==================================

Attribute directives
--------------------

They look like normal HTML directives and only change the element they are added to.

Structural Directives
---------------------

They also look like HTML directives, but start with a \*. They affect the whole area where the element(s) are used with the directive in the DOM.
Only 1 structural HTML directive is allowed to an element.

\*ngIf
-----
Conditionally add the element where the \*ngIf attribute bolongs to.

\*ngFor
------
Loop to add multiple elements where the \*ngFor attribute bolongs to.

\*ngClass
--------
Set class attribute in the element, but with some condition added to it.

\*ngStyle
--------
Set style attribute in the element, but with some condition added to it.

Create own directives
---------------------

Create a class under basic-highlight with the name basic-highlight.directive.ts .

```
@Directive({
  selector: '[appBasicHighlight]'
})
export class BasicHighlightDirective implements OnInit {
  constructor(private elmentRef: ElementRef) {
  }

  ngOnInit(): void {
    this.elmentRef.nativeElement.style.backgroundColor = 'green';
  }
}
```

In detail:
```
  constructor(private elmentRef: ElementRef) {
```
The 'private' infront of the argument is TypeScript, which will automatically create a property to the class.

```
@Directive({
  selector: '[appBasicHighlight]'
})
```
Use the []-brackets to make it accesible as an attribute in an element, but without the [].

To use it, you need to import it into app.module.ts in the declaration section.
```
@NgModule({
  declarations: [
    AppComponent,
    BasicHighlightDirective
  ],
...

```

Now you can use it in the HTML code:

```
      <p appBasicHighlight>Style me with my directive</p>
```

Bettter implementation is not to use  this.elmentRef.nativeElement.style.backgroundColor, use it throough the Renderer2 class.
Using rederer2 is also able to use it without the DOM. So the Renderer class must be investigated in detail.

```
import {Directive, OnInit, ElementRef, Renderer2} from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {

  constructor(private elRef: ElementRef, private renderer: Renderer2) { }

  ngOnInit(): void {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');
  }

}
```

HostListener
------------
When we want to make it more dynamic, use the @HostListener() event listener. The argument is the event you want to listen to.

```
  @HostListener('mouseenter') mouseover(eventData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');
  }

  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'transparent');
  }
}
```

HostBinding
-----------
Easier way attach to attributes of the element by using @HostBinding().

```
  @HostBinding('style.backgroundColor') backgroundColor: string = 'transparent';
```
Access style property and the n the sub property background Color

Use it in your class.
```
  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.backgroundColor = 'transparent';
  }
```

Combination of HostListener and HostBinding you can do nice dynamic stuff.

Adding parameter to directive
-----------------------------

Using custom property binding.

```
  @Input() defaultColor: string = 'transparent';
  @Input() highlightColor: string = 'blue';
```

Use it in the class

```
  @HostListener('mouseenter') mouseover(eventData: Event) {
    this.backgroundColor = this.highlightColor;
  }
```
Use it in the attribute

```
      <p appBetterHighlight [defaultColor]="'yellow'" [highlightColor]="'red'">Style me with my directive</p>
```

You can also set an alias to the name of the directive.

```
  @Input('appBetterHighlight') highlightColor: string = 'blue';
```
Now you can use it as follows
```
      <p [appBetterHighlight]="'red'" [defaultColor]="'yellow'">Style me with my directive</p>
```

Structural Directives
---------------------
Angular uses ng-template with ngIf or ngFor to replace the \*ngIf and the \*ngFor.

```
        <div *ngIf="!onlyOdd">
          <li
            class="list-group-item"
            *ngFor="let even of evenNumbers"
            [ngStyle]="{backgroundColor: even % 2 !== 0 ? 'yellow' : 'transparent'}"
            [ngClass]="{odd: even % 2 !== 0 }">
            {{ even }}
          </li>
        </div>
        <!-- is equal to -->
        <ng-template [ngIf]="!onlyOdd">
          <div>
            <ng-template ngFor let-even   [ngForOf]="evenNumbers">
              <li
                class="list-group-item"
                [ngStyle]="{backgroundColor: even % 2 !== 0 ? 'yellow' : 'transparent'}"
                [ngClass]="{odd: even % 2 !== 0 }">
                {{ even }}
              </li>
            </ng-template>
          </div>
        </ng-template>
```

So building out own structural directive.
Create a directive (ng c d *name*). For example ng c d unless

```
import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {

  // This is a setter
  // Must be the same name to be called from the ng-template
  @Input() set appUnless(condition: boolean) {
    if (!condition) {
      this.vcRef.createEmbeddedView(this.templateRef);
    } else {
      this.vcRef.clear();
    }

  }

  // TemplateRef reference to the template ng-template
  // ViewContainerRef reference where the template must be placed
  constructor(private templateRef: TemplateRef<any>, private vcRef: ViewContainerRef) {

  }
}
```

To use it:
```
        <div *appUnless="onlyOdd">
          <li
            class="list-group-item"
            *ngFor="let even of evenNumbers"
            [ngStyle]="{backgroundColor: even % 2 !== 0 ? 'yellow' : 'transparent'}"
            [ngClass]="{odd: even % 2 !== 0 }">
            {{ even }}
          </li>
        </div>
```



ngSwitch, \*ngSwitchCase and \*ngSwitchDefault
--------------------------------------------
Just an example explains everything:
It is steeered through the 'value' in the corresponding class.

```
      <div [ngSwitch]="value">
        <p *ngSwitchCase="5">Value is 5</p>
        <p *ngSwitchCase="10">Value is 10</p>
        <p *ngSwitchCase="100">Value is 100</p>
        <p *ngSwitchDefault>Value is Default</p>
      </div>
```