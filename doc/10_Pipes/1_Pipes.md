Pipes
=====

Introduction
------------

Used to transform data.

We have build in pipes like 'uppercase'.

So to transform to uppercase, we use a pipe symbol '\|' :

```
{{ name | uppercase }}
```
Pipes are mainly used in templates

Giving parameters to pipes to configure the format.
```
          {{ server.started | date:'fullDate' }}
```
With multiple parameters you add ':' after the first parameter.
You can go to the documentation of Angular and search for 'pipe' to find all information of different pipes.

You can also combine pipes, through chaining pipes and the order plays an important role here.

```
          {{ server.started | date:'fullDate' | uppercase }}
```
The pipes get executed from left to right.

Creating custom pipe
--------------------

To create a pipe create a file 'pipename'.pipe.ts and create a class which implements PipeTransform.
The name of the pipe to use it is put in the decorator 'Pipe' in front of the class.

```
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {
  transform(value: any): any {
    if (value.length > 10) {
      return value.substr(0, 10) + ' ...';
    }
    return value;
  }
}

```

To use the pipe, we need to add it to app.module.ts to the declarations.

```
@NgModule({
  declarations: [
    AppComponent,
    ShortenPipe
  ],
  imports: [
...
```

To use parameters, just add a parameter to the function.

```
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {
  transform(value: any, limit = 10): any {
    if (value.length > limit) {
      return value.substr(0, limit) + ' ...';
    }
    return value;
  }
}
```

You can also use Pipes in \*ngFor, which will use the list to perform its transformations on.
For example a filter:

ts-file
```
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString: string, propName: string): any {
    if (value.length === 0 || filterString === '') {
      return value;
    }
    const resultArray = [];
    for (const item of value) {
      if (item[propName] === filterString) {
        resultArray.push(item);
      }
    }
    return resultArray;
  }

}
```

To use it:
```
        <li
          class="list-group-item"
          *ngFor="let server of servers | filter:filteredStatus:'status'"
          [ngClass]="getStatusClasses(server)">
          <span
            class="badge">
            {{ server.status }}
          </span>
          <strong>{{ server.name | shorten }}</strong> |
          {{ server.instanceType | uppercase }} |
          {{ server.started | date:'fullDate' | uppercase }}
        </li>
```

Pipes won't be rerun when Array or objects change. This is done for performance reasons, because this could lead to slow doan your app. To allow the filter pipe to recalculate the data, you add the 'pure:false' property to the decorator.

```
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter';
  pure:false
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString: string, propName: string): any {
    if (value.length === 0 || filterString === '') {
      return value;
    }
    const resultArray = [];
    for (const item of value) {
      if (item[propName] === filterString) {
        resultArray.push(item);
      }
    }
    return resultArray;
  }

}
```
This is an inpure pipe.

The 'async' pipe will show data coming from Promises.

appStatus will be set after 2 seconds, if we don't use async, we will only see the Promise object.

```
export class AppComponent {
  appStatus = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('stable');
    }, 2000);
  });
```

Usage:
```
      <h2>App Status: {{ appStatus | async }}</h2>
```

This is highly used in http calls to web services


Reference:
==========
- https://angular.io/guide/pipes
- https://angular.io/api?type=pipe