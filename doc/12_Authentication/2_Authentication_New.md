Authentication
==============

See earlier version, nothing changed that much.

Loading Spinner
---------------

A nice addition is the spinner when logging in:

component-file
```
import {Component} from '@angular/core';

@Component({
  selector: 'app-loading-spinner',
  template: '<div class="lds-ripple"><div></div><div></div></div>',
  styleUrls: ['loading-spinner.component.css']
})
export class LoadingSpinnerComponent {

}
```

CSS-file
```
.lds-ripple {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-ripple div {
  position: absolute;
  border: 4px solid #286090;
  opacity: 1;
  border-radius: 50%;
  animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
}
.lds-ripple div:nth-child(2) {
  animation-delay: -0.5s;
}
@keyframes lds-ripple {
  0% {
    top: 28px;
    left: 28px;
    width: 0;
    height: 0;
    opacity: 1;
  }
  100% {
    top: -1px;
    left: -1px;
    width: 58px;
    height: 58px;
    opacity: 0;
  }
}
```

Usage:
```
    <div *ngIf="isLoading" style="position:absolute;top:33%;left:50%;">
      <app-loading-spinner></app-loading-spinner>
    </div>
```
Puts the spinner on top of the login-page and you should disable the form using fieldset.

Using pipe + take
-----------------

Using a pipe and take functions of rxjs will subscribe to the observable take the value and unsubscribe after the number of takes.

```
    this.authService.user.pipe(take(1)).subscribe(user => {
      
    })
```

Guards
------

Important is the usage of take function to only subscribe once when the Gaurd is called.

```
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';
import {map, take, tap} from 'rxjs/operators';

@Injectable({providedIn:'root'})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authService.user.pipe(
      take(1),                      // To kill the listener
      map( user => {
        const isAuth = !!user;
        if (isAuth) {
          return true;
        } else {
          return this.router.createUrlTree(['/auth']);
        }
      })
      // ,tap(isAuth => {
      //   if (!isAuth) {
      //     this.router.navigate(['/auth']);
      //   }
      // })
    );
  }
}
```


References
----------

- https://firebase.google.com/docs/reference/rest/auth
- https://jwt.io