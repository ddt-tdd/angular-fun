Introduction
============

This uses the deprecated HttpModule of Angular 6.
HTTP request are send to the backend to store and keep the data. The response is then given back to the application.
This is AJAX calls, where we use in this module the deprecated HttpModule.

As backend we use Firebase for demo purposes.
URL: https://udemy-ng-http-a8455.firebaseio.com/data.json
data.json -> the .json Firebase knows we're using JSON data. The 'data' is the name of the storage.

Making HTTP request
-------------------

To be able to send request, we need to add the http service of Angular.

```
import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class ServerService {
  constructor(private http: Http) {}

  storeServers(servers: any[]) {
    return this.http.post('https://udemy-ng-http-a8455.firebaseio.com/', servers):
  }
}
```
Ẁe return http.port(), because this is an observable where you need to subscribe to. We will subscribe to it in the class which calls the storeServers() function, because we want to review the result.
Add the service to AppModule providers.

To use the Service in the component:
```
  onSave() {
    this.serverService.storeServers(this.servers)
      .subscribe(
        (response) => console.log(response),
        (error) => console.log(error)
      );
  }
```
We wrote the onSave function, which will subscribe for the result.

Sending Headers
---------------

Sometimes you need to send special header in the request.

```
mport { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class ServerService {
  constructor(private http: Http) {}

  storeServers(servers: any[]) {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(
      'https://udemy-ng-http-a8455.firebaseio.com/data.json',
      servers,
      {headers: headers});
  }
}
```

Getting data back
-----------------

We use the http.get() method

```
  getServers() {
    return this.http.get('https://udemy-ng-http-a8455.firebaseio.com/data.json');
  }
```

To use it
```
  onGet() {
    this.serverService.getServers()
      .subscribe(
        (response) => console.log(response),
        (error) => console.log(error)
      );
  }
```

To parse the body of the response, we can use the json() method.
```
  onGet() {
    this.serverService.getServers()
      .subscribe(
        (response: Response) => {
           const data = response.json();
           console.log(data);
        }
        (error) => console.log(error)
      );
  }
```

Overwriting the data
--------------------

We use the http.put() method. On Firebase the duplicates are gone and we have only 1 entry, which you can overwrite. So the post will add constantly new objects to Firebase

```
  storeServers(servers: any[]) {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    // return this.http.post(
    //   'https://udemy-ng-http-a8455.firebaseio.com/data.json',
    //   servers,
    //   {headers: headers});
    return this.http.put(
      'https://udemy-ng-http-a8455.firebaseio.com/data.json',
      servers,
      {headers: headers});
  }
```

Using Observables
-----------------

It would be better to do the date extraction in the Service. We can use the .pipe(map()) method. After the subscribe this methods will be called to retrieve the data from the response.
The map is imported from:
```
import {map} from 'rxjs/operators';
```

The code is:
```
  getServers() {
    return this.http.get('https://udemy-ng-http-a8455.firebaseio.com/data.json')
      .pipe(map(
        (response: Response) => {
          const data = response.json();
          return data;
        }
      ));
  }
```
Now we can show the data directly from the array. We can even change the data in the getServers() method, while we are going through it.

Catching Errors
---------------

We want ot catch the errors

```
  getServers() {
    return this.http.get('https://udemy-ng-http-a8455.firebaseio.com/data')
      .pipe(map(
        (response: Response) => {
          const data = response.json();
          return data;
        }
      ))
      .pipe(catchError(error => {
        return throwError('Something went wrong');
      }));
  }
```
In the catchError method we can transform the error to a more descriptive error.

Using async
-----------

The Service implement the app name of the application.
```
  getAppName() {
    return this.http.get('https://udemy-ng-http-a8455.firebaseio.com/appName.json')
      .pipe(map(
        (response: Response) => {
          return response.json();
        }));
  }
```

To get the observable and pass the data to a property of the component.

```
  appName = this.serverService.getAppName();
```

Using async pipe to subscribe to the observable of the http.get() call.

In the HTML code:
```
      <h1> {{ appName | async }}</h1>
```



