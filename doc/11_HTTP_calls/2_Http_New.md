Introduction
============

This uses the new HttpClient of Angular 8.
HTTP request are send to the backend to store and keep the data. The response is then given back to the application.
This is AJAX calls, where we use in this module the deprecated HttpModule.

As backend we use Firebase for demo purposes.
URL: https://ng-http-01.firebaseio.com/data.json
data.json -> the .json Firebase knows we're using JSON data. The 'data' is the name of the storage.

Anatomy of a Http Request
-------------------------

URL -> backend API endpoint.
- POST : create
- GET : read
- PUT : update
- DELETE: delete

Set Metadata into the headers of the request.

Some Http methods (POST, PUT, PATCH), you set the data in the request body

Making HTTP request
-------------------

To be able to send request, we need to add the HttpClient module of Angular to the app.module.ts.

```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```
Now the HttpClient is usable in the whole application module.
To post request we inject the HttpClient to the component who needs it. So we can use it:

```
import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadedPosts = [];

  constructor(private httpClient: HttpClient) {}

  ngOnInit() {}

  onCreatePost(postData: { title: string; content: string }) {
    // Send Http request
    this.httpClient.post(
      'https://ng-http-01.firebaseio.com/posts.json',
      postData
    ).subscribe( responseData => {
      console.log(responseData);
    });
  }

  onFetchPosts() {
    // Send Http request
  }

  onClearPosts() {
    // Send Http request
  }
}
```
The post method wont do anything without having observers subscribed to it.
The post send 2 request. First an OPTIONS method to check POST is allowed.

We return http.port(), because this is an observable where you need to subscribe to. We will subscribe to it in the class which calls the storeServers() function, because we want to review the result.

Getting data from firebase using get request.

```
  private fetchPosts() {
    this.httpClient.get('https://ng-http-01.firebaseio.com/posts.json')
      .subscribe( posts => {
        console.log(posts);
      });
  }
```

This will send it back from firebase with the identifiers infront of it. So we need to transform it into an array.
```
  private fetchPosts() {
    this.httpClient.get('https://ng-http-01.firebaseio.com/posts.json')
      .pipe(map(responseData => {
        const postArray = [];
        for (const key in responseData) {
          if (responseData.hasOwnProperty(key)) {
            postArray.push({ ...responseData[key], id: key});
          }
        }
        return postArray;
      }))
      .subscribe( posts => {
        console.log(posts);
      });
  }
```
... is the unfold operator on javascript objects.

Now we can perform type checking on the responses. Supose we have a post model. We can verify the type by adding the optional <> to the post, get, ... method and declare the response type inside of it.

```
import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {PostModel} from "./post.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadedPosts = [];

  constructor(private httpClient: HttpClient) {}

  ngOnInit() {
    this.fetchPosts();
  }

  onCreatePost(postData: PostModel) {
    // Send Http request
    this.httpClient.post<{name: string}>(
      'https://ng-http-01.firebaseio.com/posts.json',
      postData
    ).subscribe( responseData => {
      console.log(responseData);
    });
  }

  onFetchPosts() {
    // Send Http request
    this.fetchPosts();
  }

  onClearPosts() {
    // Send Http request
  }

  private fetchPosts() {
    this.httpClient.get<{ [key: string]: PostModel }>('https://ng-http-01.firebaseio.com/posts.json')
      .pipe(map(responseData  => {
        const postArray: PostModel[] = [];
        for (const key in responseData) {
          if (responseData.hasOwnProperty(key)) {
            postArray.push({ ...responseData[key], id: key});
          }
        }
        return postArray;
      }))
      .subscribe( posts => {
        console.log(posts);
      });
  }
}

```

Better to export the Http calls to services.

Post Service creation:
```
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {PostModel} from "./post.model";
import {map} from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class PostsService {

  constructor(private httpClient: HttpClient) {}

  createAnsStorePost(title: string, content: string) {
    const postData: PostModel = {title: title, content: content};

    // Send Http request
    this.httpClient.post<{name: string}>(
      'https://ng-http-01.firebaseio.com/posts.json',
      postData
    ).subscribe( responseData => {
      console.log(responseData);
    });
  }

  fetchPosts() {
    this.httpClient.get<{ [key: string]: PostModel }>('https://ng-http-01.firebaseio.com/posts.json')
      .pipe(map(responseData  => {
        const postArray: PostModel[] = [];
        for (const key in responseData) {
          if (responseData.hasOwnProperty(key)) {
            postArray.push({ ...responseData[key], id: key});
          }
        }
        return postArray;
      }))
      .subscribe( posts => {
      });
  }
}
```
To get the results from the service, we have to possibilities: use a Subject or return the observable.
We perform all the http and transforms in the service, but we return the observables.

Handling Errors
---------------

You should implement error handling to inform the user something went wrong. In the subscribe you can also add an error handler.

```
  ngOnInit() {
    this.isFetching = true;
    this.postsService.fetchPosts()
      .subscribe((responseData) => {
        this.isFetching = false;
        this.loadedPosts = responseData;
      }, error => {
        this.error = 'ERROR: ' + error.message;
        console.log(error);
      });
  }
```
More information of the error message and error objects, you need to check the web service API.

We can also handle errors using Subjects, where all actions in the subscribe are handled in the Service.

Declaration and triggering:
```
export class PostsService {
  error = new Subject<string>();

  constructor(private httpClient: HttpClient) {}

  createAnsStorePost(title: string, content: string) {
    const postData: PostModel = {title: title, content: content};

    // Send Http request
    this.httpClient.post<{name: string}>(
      'https://ng-http-01.firebaseio.com/posts.json',
      postData
    ).subscribe( responseData => {
      console.log(responseData);
    },
    error => {
      this.error.next(error.message);
    });
  }

  ...
```

Usage in the component:
```
 ngOnInit() {
    this.errorSub = this.postsService.error.subscribe( errorMessage => {
      this.error = errorMessage;
    });

    this.isFetching = true;
    this.postsService.fetchPosts()
      .subscribe((responseData) => {
        this.isFetching = false;
        this.loadedPosts = responseData;
      }, error => {
        this.error = 'ERROR: ' + error.message;
        console.log(error);
      });
  }

  ngOnDestroy(): void {
    this.errorSub.unsubscribe();
  }
```

Special operator catchError:
```
  fetchPosts() {
    return this.httpClient.get<{ [key: string]: PostModel }>('https://ng-http-01.firebaseio.com/posts.json')
      .pipe(map(responseData  => {
        const postArray: PostModel[] = [];
        for (const key in responseData) {
          if (responseData.hasOwnProperty(key)) {
            postArray.push({ ...responseData[key], id: key});
          }
        }
        return postArray;
      }),
      catchError(errorRes => {
        // send to analytics server
        // pass on to throwError which is an observable
        return throwError(errorRes);
      }));
  }
```

Setting Headers
---------------

Sometimes you need to set some special headers.
```
 fetchPosts() {
    return this.httpClient.get<{ [key: string]: PostModel }>('https://ng-http-01.firebaseio.com/posts.json',
      {
        headers: new HttpHeaders({'Custom-header': 'hello'})
      })
      .pipe(map(responseData  => {
        const postArray: PostModel[] = [];
...
```

Adding Query parameters
-----------------------

```
    return this.httpClient.get<{ [key: string]: PostModel }>('https://ng-http-01.firebaseio.com/posts.json',
      {
        headers: new HttpHeaders({'Custom-header': 'hello'}),
        params: new HttpParams().append('print', 'pretty').append('custom', 'key')
      })
...
```

Observing different types of responses
--------------------------------------

Here we check the returns from the HTTP call.

```
  createAnsStorePost(title: string, content: string) {
    const postData: PostModel = {title: title, content: content};

    // Send Http request
    this.httpClient.post<{name: string}>(
      'https://ng-http-01.firebaseio.com/posts.json',
      postData,
      {
        observe: 'response'
      }
    ).subscribe( responseData => {
      console.log(responseData);
    },
    error => {
      this.error.next(error.message);
    });
  }
```
If we observe the response, we get the full response back with headers and body, etc ...

We can also observe events, were we can use a pipe to 'tap' into the events which are fired.
```
  deletePosts() {
    // Send Http request
    return this.httpClient.delete<{name: string}>(
      'https://ng-http-01.firebaseio.com/posts.json',
      {
        observe: 'events'
      }
    ).pipe(tap(event => {
      console.log(event);
    }));
  }
```

You have different event.type using numbers, which are the different states during the HTTP call. There you can perform different function calls.
```
  deletePosts() {
    // Send Http request
    return this.httpClient.delete<{name: string}>(
      'https://ng-http-01.firebaseio.com/posts.json',
      {
        observe: 'events'
      }
    ).pipe(tap(event => {
      console.log(event);
      if (event.type === HttpEventType.Sent) {
        console.log('messaged sent');
      }
      if (event.type === HttpEventType.Response) {
        console.log(event.body);
      }
    }));
  }
```

You can also change the reponseType like json, text, etc....

```
   this.httpClient.post<{name: string}>(
      'https://ng-http-01.firebaseio.com/posts.json',
      postData,
      {
        observe: 'response',
        responseType: 'text'
      }
    ).subscribe( responseData => {
      console.log(responseData);
    },
    error => {
      this.error.next(error.message);
    });
```

The default responseType is 'json'.

Interceptors
------------

What if you want to add a header to all the http request for authentication for example, then you can use interceptors. Intercept the request call before it is sent.

```
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";

export class AuthInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('Requets is on its way');
    return next.handle(req);
  }
}
```

You have to declare it in the app.module.ts in a specific manner.
```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import {AuthInterceptorService} from "./auth-interceptor.service";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, FormsModule, HttpClientModule],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

HTTP_INTERCEPTORS will use this in all http requests.
You control all in the interceptor.
Inside the interceptor you can modify the object, but you have to work on a copy and give it to the handler.

```
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";

export class AuthInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('Requets is on its way');
    const modifierRequest = req.clone({headers: req.headers.append('Auth', 'wyz')});
    return next.handle(modifierRequest);
  }
}
```

You can also intercept responses:

```
import {HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";

export class AuthInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('Requets is on its way');
    const modifierRequest = req.clone({headers: req.headers.append('Auth', 'wyz')});
    return next.handle(modifierRequest).pipe(tap(event => {
      console.log(event);
      if (event.type === HttpEventType.Response) {
        console.log('Response arrived');
      }
    }));
  }
}
```

The order of multiple interceptors will be important in the providers of the module.

Useful information:

Official Docs: https://angular.io/guide/http

Put request will not add, but create and always renew the object you send.
