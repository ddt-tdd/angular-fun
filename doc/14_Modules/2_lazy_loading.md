Lazy Loading
============

Optimization concept using lazy loading, which increase the startup process. Multiple modules are a prerequisite for lazy loading.
According to the routes, we can implement lazy loading. Only load the modules for the routes. Only when we visit the other modules, then they will be loaded.

For lazy loading to work you should make the sub-path into the calling routing table and use the 'loadChildren'.

recipe routing
```

const routes: Routes = [
  { path: '', component: RecipesComponent, canActivate: [AuthGuard], children: [
      { path: '', component: RecipeStartComponent },
      { path: 'new', component: RecipeEditComponent },
      { path: ':id', component: RecipeDetailComponent, resolve: [RecipesResovlerService] },
      { path: ':id/edit', component: RecipeEditComponent, resolve: [RecipesResovlerService] },
    ] },
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RecipesRoutingModule {

}

```

app routing
```
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RecipesModule} from "./recipes/recipes.module";

const appRoutes: Routes = [
  { path: '', redirectTo: '/recipes', pathMatch: 'full' },
  { path: 'recipes', loadChildren: './recipes/recipes.module#RecipesModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
```

You need to remove the import of the RecipeModule ofcourse from the AppModule.
```
NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ShoppingListModule,
    SharedModule,
    CoreModule,
    AuthModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Optimizing Lazy Loading
-----------------------

We can pre-load lady loaded modules.

With PreloadAllModules, we can load all modules, but the initial page is already loaded. This is mainly used in the app module.

```
const appRoutes: Routes = [
  { path: '', redirectTo: '/recipes', pathMatch: 'full' },
  { path: 'recipes', loadChildren: './recipes/recipes.module#RecipesModule' },
  { path: 'shopping-list', loadChildren: './shopping-list/shopping-list.module#ShoppingListModule' },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
```
You can also define your own pre-loading strategy to optimize the loading strategy.

Services & Modules
------------------

We can provide services in:
- AppModules (providers array) -> Service available application wide -> uses root injector
- AppComponents (providers array) -> Service available to component and child components in the tree -> uses component injector
- Eager-loaded Modules @Injetable(providedIn) -> Service available in app-wide -> uses root injector, but this should be avoided.
- Lazy-loaded Modules @Injetable(providedIn) -> Service available in loaded module, when it was loaded. This can be undesireable. -> Child injector. -> Only used in the module and nowhere else, be careful.

The services are singletons.
Eager-loaded modules are the modules in the import statements.
When Service are provided in the app-module and the lazy-loaded module, they each have their own instance. This can be requested behavior, but in general this can be not the meaning. In general it is better to provide services in the app-module and not in lazy loaded.
When setting Services in the shared module, then the efect will be that app-modules has its instance of the service, but also each lazy-loaded module has its own instance of the service if it loads the shared module. This can lead to bugs, be careful!!
In general:
- No Services in shared modules!!
- Service only in Core.module.ts!!

Ahead-of-Time compilation
-------------------------

Normal process of compilation is JIT:

Code + Templates -> Typescrypt compiler -> Angular compiler (in the browser)

It would be better to Ahead of Time compilation:

Code + Templates -> Typescrypt compiler -> Angular compiler (on the server or during build precess)

The JIT process is good during development time.
For production you can run:
```
>ng build --prod
```

References:
-----------
- https://angular.io/guide/ngmodules
- https://angular.io/guide/ngmodule-faq

