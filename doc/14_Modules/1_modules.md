Modules
=======

Introduction
------------

Angular modules uses ngModule, can be used as optimization and prepare of deployment.
Angular modules are not the same as Javascript modules (use import statements).
Bundling a group of Components, Directives, Service together into a NgModule. This let Angular understand the what is inside the Module.
Angular uses at least 1 module: the root module to start your application.
All core functionality of Angular are located in modules. (ex FormsModule, etc...)
Every module works on its own, so you need to export modules to become available in order modules. 

App Module (root)
-----------------

We already have used modules:
- App module
- Routing module

We have a long list of declaration of components, directives and pipes.
We have an imports array, where we import other modules to become useable in out module and application. A module which is imported will also import all its components, directives, pipes, etc... into your module, so it becomes useable in your module/application.
We have the providers array declares all services, this makes all your services injectable. You can also make them inectable if you add @Injectable( providedIn: 'root') declaration to your service.
The bootstrap array is used to start your application. You can start multiple components if necessary.
At the end we have the entryComponents array, where you declare components which are dynamically generated.

Routing Module
--------------

Here we declared the routing table used in our Angular application. This can be added to the App module, but it make the App module more cumbersome. So it is cleaner to create a seperate module for the routing declarations.
The routing module imports the RouterModule and extends it with its routing in formation. The it will export the RouterModule, contianing its routing table and configuration.

Splitting in Modules
--------------------

The bigger your application becomes, you may split your application in multiple modules. This will also enhance the speed of the application.
You split your application into a AppModule and into Feature modules, which each perform a kind of feature into the whole application.
Grouping the components into modules makes it more manageable when you work in teams or you have multipke periods of working between the apps.

In the demo project, we have 3 main feature areas: recipes, shopping list and auth area.

Creating a Module
-----------------

Create a file \<name\>.module.ts
You migrate the declarations from the app module to the new module.
Now all declarations are in the new module, we need to export them to be used in the App module. You need the verify, which one is realy used in the App-module.
You will also need to import the necessary modules into the new module to exists on it own. You don't need to reimport services module like the HttpClientModule. You also don't need to import the BrowserModule, this can only be imported AppModule. We will import the BrowserModule replacement for Feature modules, which is called the CommonModule (containd ngFor, ngIf). 

```
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {RecipesComponent} from './recipes.component';
import {RecipeListComponent} from './recipe-list/recipe-list.component';
import {RecipeDetailComponent} from './recipe-detail/recipe-detail.component';
import {RecipeItemComponent} from './recipe-list/recipe-item/recipe-item.component';
import {RecipeStartComponent} from './recipe-start/recipe-start.component';
import {RecipeEditComponent} from './recipe-edit/recipe-edit.component';


@NgModule({
  declarations: [
    RecipesComponent,
    RecipeListComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    RecipeStartComponent,
    RecipeEditComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    RecipesComponent,
    RecipeListComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    RecipeStartComponent,
    RecipeEditComponent,
  ]
})
export class RecipesModule {

}
```
Whatever you declare, you need to import the necessary modules to compile it, except the services. They will be available through the whole module.

Adding Routes
-------------
Next to components, you need to add the routes into the feature module using the RouterModule.forChild() command.

```
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RecipesComponent} from './recipes.component';
import {AuthGuard} from '../auth/auth.guard';
import {RecipeStartComponent} from './recipe-start/recipe-start.component';
import {RecipeEditComponent} from './recipe-edit/recipe-edit.component';
import {RecipeDetailComponent} from './recipe-detail/recipe-detail.component';
import {RecipesResovlerService} from './recipes-resovler.service';

const routes: Routes = [
  { path: 'recipes', component: RecipesComponent, canActivate: [AuthGuard], children: [
      { path: '', component: RecipeStartComponent },
      { path: 'new', component: RecipeEditComponent },
      { path: ':id', component: RecipeDetailComponent, resolve: [RecipesResovlerService] },
      { path: ':id/edit', component: RecipeEditComponent, resolve: [RecipesResovlerService] },
    ] },
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RecipesRoutingModule {

}
```

Because we moved the routes also to the new module, it doesn't need to export the components anymore. The feature module is completely self-sufficient.

```
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {RecipesComponent} from './recipes.component';
import {RecipeListComponent} from './recipe-list/recipe-list.component';
import {RecipeDetailComponent} from './recipe-detail/recipe-detail.component';
import {RecipeItemComponent} from './recipe-list/recipe-item/recipe-item.component';
import {RecipeStartComponent} from './recipe-start/recipe-start.component';
import {RecipeEditComponent} from './recipe-edit/recipe-edit.component';
import {RecipesRoutingModule} from './recipes-routing.module';


@NgModule({
  declarations: [
    RecipesComponent,
    RecipeListComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    RecipeStartComponent,
    RecipeEditComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    ReactiveFormsModule,
    RecipesRoutingModule
  ],
})
export class RecipesModule {

}
```

Shared Modules
--------------

In shared module we put common components into a module to share it between other modules.
The declaration of components can be done only once. So you have to check all modules to see you have only 1 declaration of a component.

Core Module
-----------

The core module is to make the AppModule cleaner and leaner. We declare all the Services into the core module and import the core module back into the App module.
This won't work for all services when you use in the @Injectable({providedIn:'root'})





