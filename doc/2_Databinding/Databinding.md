Databinding
===========

String Interpolation
--------------------

Use {{ }} where a typescript expression is entered in between, but doesn't support block expressions or multiline expressions. It also works with methods.

For example
ts-file
```
class MyComponent {
	componentId: String = "test"
}
```

html-file
```
<p>Show Component ID {{ componentId }}</p>
```

Use String interpolation for outputting data.

Property Binding
----------------

Binding property of DOM element with your implementation class.

For example:

ts-file
```
class MyComponent {
	disableButton: boolean = false;
}
```

html-file
```
<button [disabled]="disableButton">Submit</button>
```

Use property binding when changing properties.

Event Handling
--------------

Listen to events which are standard in Angular and HTML5.

For Example:

ts-file
```
class MyComponent {
	onSubmit() {
		console.log("Submit clicked.")
	}
}
```

html-file
```
<button (click)="onClick()">Submit</button>
```

For example input field:

```
class MyComponent {
	onKeyClicked(event) {
		console.log(<HTMLInputElement>(even.target).value);
	}
}
```

html-file
```
<input (input)="onKeyClicked($event)">Submit</button>
```

Two-way Databinding
-------------------

To use two-way databinding, you need `import { FormsModule } from '@angular/forms';` in the app.module.ts
Combining property and event binding [( )]
Here we use ngModel to show the data.

```
class MyComponent {
	componentId: String = "test"
}
```

html-file
```
<input [(ngModel)]="componentId">
<p>Show Component ID {{ componentId }}</p>
```

componentId of the class is used in the HTML. When you type something in the <input>-field, then componentId of class is updated.