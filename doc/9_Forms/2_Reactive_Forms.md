Reactive Forms
==============

To manually create reactive forms, we use the ReactiveFormModule and FormGroup.

To initialize the FormGroup, we do this in ngOnInit().
We create 'new FromGroup()' with a Javascript object containing the FormControl objects. This FormControl object takes 3 parameters: initial state, synchronous validator and asynchronous validator.

Example for Reactive group
```
  ngOnInit(): void {
    this.signupForm = new FormGroup({
      'username': new FormControl(null),
      'email': new FormControl(null),
      'gender': new FormControl('male')
    });
  }
```

We need to synchronize the HTML form with this FormGroup. The 'form' in the HTML file will be a normal form, so we need to add directives to make it an Angular form and connect it to the FormGroup in the class. This is property binding. We also need to bind the fields of the form with properties of the object given to the FormGroup constructor.

```
      <form [formGroup]="signupForm">
        <div class="form-group">
          <label for="username">Username</label>
          <input
            type="text"
            id="username"
            formControlName="username"
            class="form-control">
        </div>
```

You can also use property binding for the formControlName.

Submitting the form using ngSubmit binding to function onSubmit().
Here we can reuse the FormControl we created the form with.
In the FormControl object, we see the input data set to the FormControl objects passed in the Javascript object in the FormControl constructor. You find it in the value property.

```
  onSubmit() {
    console.log(this.signupForm);
  }
```

Adding Validation
-----------------

The validation can here not be implemented in the template, you need to do it in the code.
The validation can be added in the second parameter in the FormControl using the default validators of the Validator object like Validator.required, Validator.email, etc

```
  ngOnInit(): void {
    this.signupForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'gender': new FormControl('male')
    });
  }
```

To display a message if validation is not fulfilled, we can use ngIf.
```
          <input
            type="text"
            id="username"
            formControlName="username"
            class="form-control">
          <span
            *ngIf="!signupForm.get('username').valid && signupForm.get('username').touched"
            class="help-block">Please enter a valid username</span>
```
We can also reuse the ng-... classes to style the FormControls.

FormGroup can be used to nest data and bring more structure in the component.

Code:
```
  ngOnInit(): void {
    this.signupForm = new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, Validators.required),
        'email': new FormControl(null, [Validators.required, Validators.email])
      }),
      'gender': new FormControl('male')
    });
  }
```
We nest it using FromGroupName directive.
html:
```
        <div formGroupName="u serData">
          <div class="form-group">
            <label for="username">Username</label>
            <input
              type="text"
              id="username"
              formControlName="username"
              class="form-control">
            <span
              *ngIf="!signupForm.get('userData.username').valid && signupForm.get('userData.username').touched"
              class="help-block">Please enter a valid username</span>
          </div>
          <div class="form-group">
            <label for="email">email</label>
            <input
              type="text"
              id="email"
              formControlName="email"
              class="form-control">
            <span
              *ngIf="!signupForm.get('userData.email').valid && signupForm.get('userData.email').touched"
              class="help-block">Please enter a valid email</span>
          </div>
        </div>

```

FormArray
---------

A FormArray is used to control and add a list of identical form parts, which can be added or removed. You pass on the FormArray constructor an array of FormControls or an empty array.
To use the FormArray from the FormGroup, you need to cast it <FormArray>(...).

```
    this.signupForm = new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, Validators.required),
        'email': new FormControl(null, [Validators.required, Validators.email])
      }),
      'gender': new FormControl('male'),
      'hobbies': new FormArray([]),
    });
...
  onAddHobby() {
    const control = new FormControl(null, Validators.required());
    (<FormArray>this.signupForm.get('hobbies')).push(control);
  }
```
We need to synchronise it to the HTML form.

```
       <div formArrayName="hobbies">
          <h4>Your hobbies</h4>
          <button
            class="btn btn-default"
            type="button"
            (click)="onAddHobby()">Add Hobby</button>
          <div
            class="form-group"
            *ngFor="let hobbyControl of signupForm.get('hobbies').controls; let i = index;">
            <input type="text" class="form-control" [formControlName]="i">
          </div>
        </div>
```

Custom validators
-----------------

A validator is just a custom function, that's getting executed. It takes a FormControl as a parameter and returns a Javascript object containing the list of values which are true or false (boolean). If the validation is successful you have to return null.

```
  forbiddenName(control: FormControl): {[s: string]: boolean} {
    if (this.forbiddenUsernames.indexOf(control.value) !== -1) {
      return {'nameIfForbidden': true};
    }
    return null;
  }
```

To pass the validator, you sometimes need the bind the 'this' reference to the validator, because it is called outside the context of the class.

```
    this.signupForm = new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, [Validators.required, this.forbiddenName.bind(this)]),
        'email': new FormControl(null, [Validators.required, Validators.email])
```

To use the error-code, it is found on the individual FormControl in the error property. This error property can be used to give more detailed information.

```
            <span
              *ngIf="!signupForm.get('userData.username').valid && signupForm.get('userData.username').touched"
              class="help-block">
              <span *ngIf="signupForm.get('userData.username').errors['nameIsForbidden']">This name is invalid!</span>
              <span *ngIf="signupForm.get('userData.username').errors['required']">This field is required!</span>
            </span>
```

This can also be used with ngSwitch ofcourse.

Custom async validators
-----------------------

An async validator is used when an action is taking a long time.

```
  forbiddenEmail(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>((resolve, reject) => {
      setTimeout(() => {
          if (control.value === 'test@test.com') {
            resolve({'emailIsForbidden': true});
          } else {
            resolve(null);
          }
        }, 1500);
    });
    return promise;
  }
```

Used in the third parameter of the FormControl

```
...
        'username': new FormControl(null, [Validators.required, this.forbiddenName.bind(this)]),
        'email': new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmail)
      }),
...
```

Listen to status changes
------------------------

You saw in the FormControl that the state changes from ng-invalid -> ng-pending -> ng-valid. This Form state can be tracked.
We can listen to 2 observables: statusChanges and valueChanges by subscribing to it.

In ngOnInit
```
    // this.signupForm.valueChanges.subscribe((value) => {
    //   console.log(value);
    // });
    this.signupForm.statusChanges.subscribe((status) => {
      console.log(status);
    });
```

Set value and patch the value
-----------------------------

Setting a value must correspond with the form object.

```
    this.signupForm.setValue({
      'userData': {
        'username': 'David',
        'email': 'David@test.com'
      },
      'gender': 'male',
      'hobbies': []
    });
```

Patching a value.

```
    this.signupForm.patchValue({
      'userData': {
        'username': 'Anna',
      },
    });
```

Resetting the form

```
    this.signupForm.reset();
```


Note: Better validation to use is using invalid and check the dirty
```
              *ngIf="!signupForm.get('userData.username').invalid && (signupForm.get('userData.username').dirty || signupForm.get('userData.username').touched)"
              class="help-block">
              <span *ngIf="signupForm.get('userData.username').errors['nameIsForbidden']">This name is invalid!</span>
              <span *ngIf="signupForm.get('userData.username').errors['required']">This field is required!</span>
```

To set buttons disabled it is better to use the !....valid approach:
```
        <button class="btn btn-primary" type="submit" [disabled]="!projectStatusForm.valid">Button</button>
```

Why?

Initial state of the form is:

|Initial State|Pending State|Final State       |
|-------------|-------------|------------------|
|invalid:true |invalid:false|invalid:true/false|
|pending:false|pending:true |pending:false     |
|valid:false  |valid:false  |valid:false/true  |


