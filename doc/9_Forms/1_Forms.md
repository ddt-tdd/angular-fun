Forms
=====

Introduction
------------

Angular helps to process and validate forms.

```
<form>
	<label>Name</label>
	<input type="text" name="name">
	<label>Mail</label>
	<input type="text" name="email">
	<button type="submit">Save</button>
</form>
```

Translated to:

```
{
	value: {
		name: 'Max',
		email: 'max@gmail.com'
	},
	valid: true
}
```

Template vs Reactive
--------------------

###Template driven

Form is described in the template, the object infers the Form object from the DOM.

###Reactive approach

More complex, but Angular helps here alot. The structure of the form is described in the TypeScript code. The HTML code is setup and manualy the Typescript code and the HTML code is connected. You have here a greater control to fine tune the form experience.

Template Driven Forms
---------------------

In app.module.ts you need to import the FormsModule to add the form process into Angular.
Angular will new be able to detect the <form> element in the HTML code and process the sub-elements, but not automatically. It needs to learn where the input, select, etc elements are, because you may not want it to detect all the forms elements. So we add some meta-data to it to register it, this is done with ngModel. ngModel is a directive of the forms model to contol the forms. Now it is register, we have to give it a name to create it into the typescript/javascript object/representation.

```
      <form>
        <div id="user-data">
          <div class="form-group">
            <label for="username">Username</label>
            <input
              type="text"
              id="username"
              class="form-control"
              ngModel
              name="username">

```
Here we register the input field "username" with the name "username".

The behaviour of the button must be added to submit the data, but not with standard behaviour doing an HTTP request. It must be catched by our Angular program. This is done by lisening to the ngSubmit event, added to the 'form' element.

```
      <form (ngSubmit)="onSubmit()">
        <div id="user-data">
          <div class="form-group">
            <label for="username">Username</label>
...
        <button class="btn btn-primary" type="submit">Submit</button>
      </form>
```

In the component code:

```
  onSubmit() {
    console.log('Submitted');
  }
```

To get access to the form object, add a reference #f="ngForm". The ngForm will retreive the form object and assign it to the reference #f. The reference is then passed to your submit function.

```
      <form (ngSubmit)="onSubmit(f)" #f="ngForm">
```

In the function, you can access it through the NgForm object.

```
  onSubmit(form: NgForm) {
    console.log(form);
  }
```

This is an NgForm  object, where the 'value' property contains the form object.
Except of the value, it has a lot of other propertie, which can be read. For example: dirty -> one of the values changed, valid/invalid -> depends on validators, disabled -> check if form is disabled, touched -> clicked on one of the elements.

We can also access the reference using @ViewChild and not passing it through the submit.

Validation
----------

Using HTML5 and Angular validators like: required, email. Add them to the form template. When you submit, you can check the valid property of the NgForm object.
The validation is tracked on element level and form level. Angular also adds classes to the elements, like ng-dirty, ng-touched, ng-valid, etc... So you can style the element according to its validation state.
You can use the ngForm reference and the state properties in ngIf, etc...

```
      <form (ngSubmit)="onSubmit(f)" #f="ngForm">
```

To check on an element and display error message.

```
            <input
              type="email"
              id="email"
              class="form-control"
              ngModel
              name="email"
              required
              email
              #email="ngModel">
            <span class="help-block" *ngIf="email.invalid && email.touched">Please enter a  valid email!</span>
```

Validation using the pattern attribute and regular expression
```
          <input
            type="number"
            id="amount"
            class="form-control"
            ngModel
            name="amount"
            required
            pattern="^[1-9]+[0-9]*$"
          >
```

Default values
--------------

We can use the ngModel in property binding mode to set default values.

Set the default value in the ts-file
```
export class AppComponent {
  @ViewChild('f') signupForm: NgForm;
  defaultQuestion = 'pet';
```

Bind ngModel to the property in the component
```
        <div class="form-group">
          <label for="secret">Secret Questions</label>
          <select
            id="secret"
            class="form-control"
            [ngModel]="defaultQuestion"
            name="secret">
            <option value="pet">Your first Pet?</option>
            <option value="teacher">Your first teacher?</option>
          </select>
        </div>
```

Use two-way binding
-------------------

Two-way binding is also supported on form elements using [(ngModel)]="anwser", where answer is a property of the component.

Group the ngForm object
-----------------------

To group data in your ngForm object, you can use an enclosing div and add ngModelGroup with an identifier
```
        <div id="user-data" ngModelGroup="userData">
```
Creates
```
{
	userData: {
		...
	},
	...
}
```

This will also reflect to the controls property of ngForm. This div also gets the ng-... styling directives, which can be used for styling. and so fort.

With the following
```
        <div id="user-data" ngModelGroup="userData" #userData="ngModelGroup">
...
        <p *ngIf="!userData.valid && userData.touched">User Data is invalid</p>

```
We can use the reference userData to check the validity of the group elements.

Radio buttons
-------------

In the ts-file:
```
    genders = ['male', 'female'];
```

To create the radio buttons:
```
        <div class="radio" *ngFor="let gender of genders">
          <label>
            <input type="radio" name="gender" ngModel [value]="gender" required>{{ gender }}
          </label>
        </div>
```

Set values on controls
----------------------

To set value on all controls, we can the setValue command on the signupForm, which is a reference to NgForm. We need to give the whole form object

```
    suggestUserName() {
      const suggestedName = 'Superuser';
      this.signupForm.setValue({
        userData: {
          username: suggestedName,
          email: ''
        },
        secret: 'pet',
        questionAnswer: '',
        gender: 'female'
      });
```

Is reclick on the suggested username, it resets all entries, because we set the value of an exact copy of the form object.
To patch only the username we use patchValue on the 'form' property of form object.

```
   suggestUserName() {

      this.signupForm.form.patchValue({
        userData: {
          username: suggestedName
        }
      });
```

Resetting forms
---------------

On the NgForm call the reset, which not only reset the values, but also the states of the ngForm object.

```
      this.signupForm.reset();
```



