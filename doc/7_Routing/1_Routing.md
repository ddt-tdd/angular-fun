Routing
=======

Display several pages using routing within Angular using the Router module.
Creating seperate pages with different URLs using routes.

Routes
------
Declare the URLs with the Angular components it should show.
This is defined in the app.module.ts above the ngModule:

```
const appRoutes: Routes = [
  { path: 'users', component: UsersComponent } // http://localhost:4200/users
];
```

The 'path' is the always the root-path from the application.
The 'component' is the Angular component, which must be called.

The routes must be registered in the app. Add the RouterModule and register the constant, using the method forRoot() of the RouterModule.

```
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
  ],

```

Angular can't show the router selection, because it needs a place to display the selection of the router. This can be done in the main page of the app.component.html .

```
  <div class="row">
    <div class="col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">
      <router-outlet></router-outlet>
    </div>
  </div>
```

routerLink
----------

Usage of links in the application.
If we use the href='', then the application will reload each time. This is not the idea of using Angular as a SPA. Don't use href.
Need a special directive RouterLink.

```
        <li role="presentation"><a routerLink="/servers">Servers</a></li>
```

We can also use property binding to RouterLink, which need to evaluate the expression between the double quotes.

```
        <li role="presentation"><a [routerLink]="'/users'">Servers</a></li>
```

The router link without a slash (/) will be a relative path, which always appends to the current path. With relative paths you can also use the .. to go one level up.
/ -> root or absolute path
.. -> go to parent path
./ -> stay on the current path like no /

Styling the routes
------------------

Set dynamicly the style of the selected route using routerLinkActive which will define a class.

```
        <li role="presentation" routerLinkActive="active"><a routerLink="/">Home</a></li>
```
Here the 'active' class will be set, but because of the / the home stays active. The / is also contained in /servers and /users.
So the 2 CSS styles are added together.

```
        <li role="presentation" routerLinkActive="active" [routerLinkActiveOptions]="{exact: true}"><a routerLink="/">Home</a></li>
```
The routerLinkActiveOptions, you can use the 'exact:true' option to only add the CSS class 'active' when the routerLink is exact th /.

Navigating Programmatically
---------------------------

Programmatically navigate using the Router module injected into the class through the constructor.

```
  constructor(private router: Router) { }

  onLoadServers() {
    this.router.navigate(['/servers']);
  }
```
Using the method 'navigate()' we can route to another page. We always use absolute paths by default. It router object doesn't know the current page, so it is always a absolute path with or without the /.
To use relative paths we need to use the 'relativeTo' in the navigate method as an option. The relativeTo needs the route object of ActivatedRoute.

```
  constructor(private serversService: ServersService, private router: Router, private route: ActivatedRoute) { }

  onReload() {
    this.router.navigate(['servers'], { relativeTo: this.route });
  }
```

Passing parameters using routes
-------------------------------

Sometimes the routes or URL contains parameters to be used in the application.
If we want to pass the index of the Users array. To pass a parameter you use :id, where 'id' will be the parameter passed to the component.

```
  { path: 'users/:id', component: UsersComponent },
```

We can now read the params in the component use the ActivatedRoute object, which must be injected in the component.

```
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = {
      id: this.route.snapshot.params['id'],
      name: this.route.snapshot.params['name']
    };
  }
```

This works fine use the URL and when visiting the page through the URL, but doesn't work with the routerLink directive on the component itself. This happens, because the component is not recreated. To fix this we use an observable on the ActivatedRoute 'params' observable.

In html of the component:
```
<a [routerLink]="['/users', 5, 'Peter']">Load Peter (5)</a>
```

To make this work an observable must be added to ngOnInit()
```
  ngOnInit() {
    this.user = {
      id: this.route.snapshot.params['id'],
      name: this.route.snapshot.params['name']
    };

    this.route.params.subscribe(
      (params: Params) => {
        this.user.id = params['id'];
        this.user.name = params['name'];
      }
    );
  }
```

In the background it cleans up automatically the subscription to the observable. Sometimes you need to unsubscribe it manualy.
The you need to keep the reference to the subscription object and unsubscribe on destroy of the object.


Example:
```
export class UserComponent implements OnInit, OnDestroy {
  user: {id: number, name: string};

  private routeSubscription: Subscription;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = {
      id: this.route.snapshot.params['id'],
      name: this.route.snapshot.params['name']
    };

    this.routeSubscription = this.route.params.subscribe(
      (params: Params) => {
        this.user.id = params['id'];
        this.user.name = params['name'];
      }
    );
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }
}
```

Query parameters
----------------
The parameters after ? on the URL and split with #, like

```
http://localhost:4200/users/10/Anna?mode=edit#loading
```

To add the parameters to the [routerLink], we need to add a property [queryParams] with the params needed in the routerLink.

```
      <a
        [routerLink]="['/servers', 5, 'edit']"
        [queryParams]="{allowEdit: '1'}"
        fragment="loading"
        href="#"
        class="list-group-item"
        *ngFor="let server of servers">
        {{ server.name }}
      </a>

```

You can also do it programatically:

```
 onLoadServers(id: number) {
    this.router.navigate(['/servers', id, 'edit'], { queryParams: { allowEdit: '1' }, fragment: 'loading'});
  }
```

To retrieve the parameters, we need the ActivatedRoute object and we can readout the queryParams and fragment object.

```
  constructor(private serversService: ServersService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    console.log(this.route.snapshot.queryParams);
    console.log(this.route.snapshot.fragment);

```

This happens only on component construction and not update of the component. To let that function, you need to  add observables.
```
this.route.queryParams.subscribe(...)
this.route.fragment.subscribe(...)
```

Child (Nested) Routing
----------------------

We want to use pages within components themselves. This is done through nested and child routing within the routing table using the children attribute to the routing table. The routing-outlet must than also be implemented in the components with the children routing.

```
const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'users', component: UsersComponent, children: [
      { path: ':id/:name', component: UserComponent },
    ] },
  { path: 'servers', component: ServersComponent, children: [
      { path: ':id', component: ServerComponent },
      { path: ':id/edit', component: EditServerComponent },
    ] },
];
```
Add the nested routes to Routes table.

In the HTML page must implement the router-outlet element to display the page
```
<div class="row">
  <div class="col-xs-12 col-sm-4">
    <div class="list-group">
      <a
        [routerLink]="['/users', user.id, user.name ]"
        href="#"
        class="list-group-item"
        *ngFor="let user of users">
        {{ user.name }}
      </a>
    </div>
  </div>
  <div class="col-xs-12 col-sm-4">
    <router-outlet></router-outlet>
  </div>
</div>
``` 

You can preserve the query parameters to the child pages using the queryParamsHandling (with 'merge' to merge the parameters with new query parameters and 'preserve' to only preserve the parameters) parameter.

```
    this.router.navigate(['edit'], {relativeTo: this.route, preserveQueryParams: true});
```

or

```
    this.router.navigate(['edit'], {relativeTo: this.route, queryParamsHandling: 'preserve'});
```

Redirecting and Wildcard Routes
-------------------------------

When unknown URLs are entered, it should be catched with a 404 or redirected to a default page.

###Redirecting

Path to redirect to another path.

```
  { path: 'not-found', component: PageNotFoundComponent },
  { path: 'something', redirectTo: '/not-found'}
```
If '/something' to the path, then redirected to '/not-found'.

###Wildcard

To catch all non-defined routes, we can use the wildcard \*\*. This must be the last one to catch all unknown paths.

```
...
  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/not-found'},
}
```

In our example, we didn't encounter any issues when we tried to redirect the user. But that's not always the case when adding redirections.

By default, Angular matches paths by prefix. That means, that the following route will match both /recipes  and just / 

```
{ path: '', redirectTo: '/somewhere-else' } 
```

Actually, Angular will give you an error here, because that's a common gotcha: This route will now ALWAYS redirect you! Why?

Since the default matching strategy is "prefix" , Angular checks if the path you entered in the URL does start with the path specified in the route. Of course every path starts with ''  (Important: That's no whitespace, it's simply "nothing").

To fix this behavior, you need to change the matching strategy to "full" :

```
{ path: '', redirectTo: '/somewhere-else', pathMatch: 'full' } 
```

Now, you only get redirected, if the full path is ''  (so only if you got NO other content in your path in this example).

Create External Routing Module
------------------------------

Move the constant of the routing table to a seperate file 'app-routing.module.ts'.

For example:

```
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomeComponent} from './home/home.component';
import {UsersComponent} from './users/users.component';
import {UserComponent} from './users/user/user.component';
import {ServersComponent} from './servers/servers.component';
import {ServerComponent} from './servers/server/server.component';
import {EditServerComponent} from './servers/edit-server/edit-server.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'users', component: UsersComponent, children: [
      { path: ':id/:name', component: UserComponent },
    ] },
  { path: 'servers', component: ServersComponent, children: [
      { path: ':id', component: ServerComponent },
      { path: ':id/edit', component: EditServerComponent },
    ] },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
```

And declare in the app.module.ts

```
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UsersComponent,
    ServersComponent,
    UserComponent,
    EditServerComponent,
    ServerComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
  ],
  providers: [ServersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

