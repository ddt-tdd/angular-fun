Guards
======

We want to protect some routes in the Angular apps using Guards.

canActivate
-----------

To add local authorization (must be revalidated on server side) you can create Guards, by using canActive.

auth-guard.service.ts:
```
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.authService.isAuthenticated().then((authenticated:boolean) => {
      if (authenticated) {
        return true;
      } else {
        this.router.navigate(['/']);
      }
    });
  }
}
```
This is a very basic example. The authentication service (auht.service.ts) is injected in the auth-guard.service.ts, which performs authorization to protected parts of the side. This service implements the CanActivate interface and must implement the canActivate method, which will be called by the routing table.

```
const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'users', component: UsersComponent, children: [
      { path: ':id/:name', component: UserComponent },
    ] },
  { path: 'servers', canActivate: [AuthGuard], component: ServersComponent, children: [
      { path: ':id', component: ServerComponent },
      { path: ':id/edit', component: EditServerComponent },
    ] },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/not-found'}
];
```
The canActive attribute of the routes, can use multiple guards (so it is an array).

This is implemented for the whole Servers Component. This can also be implementedd for the childs using the CanActivateChild interface.
In the routing table, the canActivate can be replaced with canActivateChild

CanDeactivate
-------------

Prevent the user to leave a page accidentally.
We have to implement an custom interface to the Component needing the warning. This interface will be called by the implementation of the CanDeactivate interface.

Implementation of CanDeactivate interface, which use the declared CanComponentDeactivate.

```
import {Observable} from 'rxjs';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot} from '@angular/router';

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
  canDeactivate(component: CanComponentDeactivate,
                currentRoute: ActivatedRouteSnapshot,
                currentState: RouterStateSnapshot,
                nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return component.canDeactivate();
  }
}
```

This CanDeactivateGuard must be added as a provider.

```
  providers: [ServersService, AuthService, AuthGuard, CanDeactivateGuard],
```

Here we implement the CanComponentDeactivate interface, which will be called by the CanDeactivate interface guard.
```
export class EditServerComponent implements OnInit, CanComponentDeactivate {
  server: {id: number, name: string, status: string};
...
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.allowEdit) {
      return true;
    }
    if ((this.serverName !== this.server.name) || (this.serverStatus !== this.server.status) && !this.changesSaved) {
      return confirm('Are you sure to leave the page?');
    } else {
      return true;
    }
  }
}
```

The CanDeactivate interface implementation will be used by the routing table:
```
const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'users', component: UsersComponent, children: [
      { path: ':id/:name', component: UserComponent },
    ] },
  { path: 'servers',
    // canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: ServersComponent, children: [
      { path: ':id', component: ServerComponent },
      { path: ':id/edit', component: EditServerComponent, canDeactivate: [CanDeactivateGuard] },
    ] },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/not-found'}
];
```

Passing data to Routes
----------------------

###Static data passing
Here we pass static data to the components using the route table.

Example a generic Error Page component

html:
```
<h4>{{ errorMessage }}</h4>
```

source:
```
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {

  errorMessage: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    // this.errorMessage = this.route.snapshot.data['message'];
    this.route.data.subscribe(
      (data: Data) => {
        this.errorMessage = data['message'];
      }
    );
  }

}

```

Usage in the routing table:
```
  { path: 'not-found', component: ErrorPageComponent, data: {message: 'Page not found!'} },
```

###Dynamic data passing
Here we want to pass dynamic data.

In this example we will simulate that we get some server data fropm the backend when we want to display it.
For such a use-cae, we need a resolver (a kind of Service). A resolver will render the component, but will first fetch the data before displaying the route.

The resolver will get the data form the ServersService.
```
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {ServersService} from '../servers.service';

interface Server {
  id: number;
  name: string;
  status: string;
}

@Injectable()
export class ServerResolver implements Resolve<Server> {

  constructor(private serversService: ServersService) {  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<Server> |
    Promise<Server> |
    Server {
    return  this.serversService.getServer(+route.params['id']);
  }
}
```

This Service must be added to the app-module providers.
```
  providers: [ServersService, AuthService, AuthGuard, CanDeactivateGuard, ServerResolver],
```

We will add this resolver to the routing module:
```
      { path: ':id', component: ServerComponent, resolve: {server: ServerResolver} },
```

The resolver will be called before the ServerComponent and will get the data for the component. The resolver will put this data into the 'data' property of the route object. Please use the observable.

On the ServerComponent, we replace the ngOnInit code with the data code which is passed using the resolver. The name of the data object must match the name used in the routing table.
```
  ngOnInit() {
    // const id = this.route.snapshot.params['id'];
    // this.server = this.serversService.getServer(parseInt(id, 10));
    // this.route.params.subscribe(
    //   (params: Params) => {
    //     this.server = this.serversService.getServer(parseInt(params['id'], 10));
    //   });
    this.route.data.subscribe(
      (data: Data) => {
        this.server = data['server']; // Must match to the name of routing table
      }
    );
  }
```

The webserver which hosts your Angular, must map all urls to the index.html page of your Angular application to show the app.
If this doesn't work, you can fall back to the # tag method. Enable it in the app-routing.module.ts
```
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {useHash: true}}
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
```