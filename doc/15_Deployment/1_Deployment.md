Deployment
==========

Prepartion
----------

- Use and check environment variables
- Polish and test the code
- Build the application for production and it should be as small as possible (ng build --prod) using ahead-of-time compilation
- Deploy build artifacts to a static host (firebase or S3 bucket)


Using environment variables
---------------------------

In the src\\environments are 2 files:
- environment.prod.ts (for production)
- environment.ts (for development)

In these files you set configuration parameters used in your application, such as an API-key. During production build the environment.prod.ts will be swapped with the environment.ts file and vice-versa. So you only import the environment file and never the environment.prod.

Example using the API-key

environment.ts
```
export const environment = {
  production: false,
  firebaseAPIKey: 'AIzaSyAzlEP32l6a6pHiOIe8agwfMpOj2W2fkPM'
};
```

Usage 
```
import { environment } from '../../environments/environment';

...
  signup(email: string, password: string) {
    return this.httpClient.post<AuthResponseData>(
      'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=' + environment.firebaseAPIKey,
      {
        email: email,
        password: password,
        returnSecureToken: true
```

Build the project
-----------------

```
ng build --prod
```

All is compiled into javascript code which updates the DOM. You find the distributable in the 'dist' folder in the sub directory of your project namd.

To deploy you can use AWS S3 and Firebase. We use Firebase here.

