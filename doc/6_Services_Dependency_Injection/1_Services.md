Services & Dependency Injection
===============================

Services are classes which store the common behabiour used in different components, modules and services.
Services can also store data centrally or manage the data centrally.
Services can be very helpful in communicating with eachother.

Create a Service
----------------
It is just a TypeScript class.

```
export class LoggingService {
  logStatusChange(status: string) {
    console.log('A server status changed, new status: ' + status);
  }
}
```

Use a Service
-------------
Getting access to the service, we will use Dependency Injection.
We add to the constructor the class to be injected

When we use it in NewAccountComponent by adding the class to be injected into the constructor.

```
import { Component, EventEmitter, Output } from '@angular/core';
import {LoggingService} from '../logging.service';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css'],
  providers: [LoggingService]
})
export class NewAccountComponent {
  @Output() accountAdded = new EventEmitter<{name: string, status: string}>();

  constructor(private loggingService: LoggingService) {}
  
  onCreateAccount(accountName: string, accountStatus: string) {
    this.accountAdded.emit({
      name: accountName,
      status: accountStatus
    });
  }
}
```
To let angular inject it we will need also add to the 'providers' to tell the Angular how to create it. We will review this later in the Hierarchical Injector chapter.
This makes it that service code can be reused DRY principle.

Use a Service to store data
---------------------------
This will make all those EventEmitters and Listeners much simpler. Through passing data through the service?
The Service looks like:

```
export class AccountsService {
  accounts = [
    {
      name: 'Master Account',
      status: 'active'
    },
    {
      name: 'Testaccount',
      status: 'inactive'
    },
    {
      name: 'Hidden Account',
      status: 'unknown'
    }
  ];

  addAccount(name: string, status: string) {
    this.accounts.push({name: name, status: status});
  }

  updateAccount(id: number, status: string) {
    this.accounts[id].status = status;
  }
}
```

To read it:
```
export class AppComponent implements OnInit {

  accounts: {name: string, status: string}[] = [];

  constructor(private accountsService: AccountsService) {}

  ngOnInit(): void {
    this.accounts = this.accountsService.accounts;
  }

}
```

To use the management functions:
```
import { Component, Input } from '@angular/core';
import {LoggingService} from '../logging.service';
import {AccountsService} from '../accounts.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  providers: [ LoggingService, AccountsService ]
})
export class AccountComponent {
  @Input() account: {name: string, status: string};
  @Input() id: number;

  constructor(private loggingService: LoggingService, private accountsService: AccountsService) {}

  onSetTo(status: string) {
    this.accountsService.updateAccount(this.id, status);
    this.loggingService.logStatusChange(status);
  }
}
```
This doesn't work, because the AccountsService is created and injected multiple times. See 'Hierarchical Injector'.

Hierarchical Injector
---------------------
Everywhere you deplare the service in the providers, it is created and instantiated. So we use the services wrong.
To create one instance, we need to create higher in the component hierarchy. This creates a kind of Singleton.
It uses a hierarchical injector:
- app.module.ts -> global injected -> Application-wide
- app.component.ts -> available in all components, but not to other services
- any other component -> available to the component and the child components.

Let's fix the app:
We remove it from the providers array and move it to the app.component.ts

Injecting Services to Services
------------------------------
Highest level is the app.module.ts. So it is able to be used in other services.
So If we want to use LoggingService into AccountService.

```
import {LoggingService} from './logging.service';
import {Injectable} from '@angular/core';

@Injectable()
export class AccountsService {
  accounts = [
    {
      name: 'Master Account',
      status: 'active'
    },
    {
      name: 'Testaccount',
      status: 'inactive'
    },
    {
      name: 'Hidden Account',
      status: 'unknown'
    }
  ];

  constructor(private loggingService: LoggingService) {}

  addAccount(name: string, status: string) {
    this.accounts.push({name: name, status: status});
    this.loggingService.logStatusChange(status);
  }

  updateAccount(id: number, status: string) {
    this.accounts[id].status = status;
    this.loggingService.logStatusChange(status);
  }
}
```
@Injectable() -> Angular knows now that it has to inject the classes into the injectable class.

Cross Component Communication
-----------------------------
We can use EventEmitter in the Service, which can emit in one component and listen in another component.

The Service:
```
import {LoggingService} from './logging.service';
import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class AccountsService {
  accounts = [
    {
      name: 'Master Account',
      status: 'active'
    },
    {
      name: 'Testaccount',
      status: 'inactive'
    },
    {
      name: 'Hidden Account',
      status: 'unknown'
    }
  ];

  statusUpdated = new EventEmitter<string>();

  constructor(private loggingService: LoggingService) {}

  addAccount(name: string, status: string) {
    this.accounts.push({name: name, status: status});
    this.loggingService.logStatusChange(status);
  }

  updateAccount(id: number, status: string) {
    this.accounts[id].status = status;
    this.loggingService.logStatusChange(status);
  }
}
```

The component emitting the event using the service
```
import { Component, Input } from '@angular/core';
import {LoggingService} from '../logging.service';
import {AccountsService} from '../accounts.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
//  providers: [ LoggingService ]
})
export class AccountComponent {
  @Input() account: {name: string, status: string};
  @Input() id: number;

  constructor(private loggingService: LoggingService, private accountsService: AccountsService) {}

  onSetTo(status: string) {
    this.accountsService.updateAccount(this.id, status);
    this.accountsService.statusUpdated.emit(status);
//    this.loggingService.logStatusChange(status);
  }
}
```

The component listening to the event from the service
```
import { Component} from '@angular/core';
import {LoggingService} from '../logging.service';
import {AccountsService} from '../accounts.service';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css'],
//  providers: [LoggingService]
})
export class NewAccountComponent {

  constructor(private loggingService: LoggingService, private accountsService: AccountsService) {
    this.accountsService.statusUpdated.subscribe((status: string) => alert('New status: ' + status));
  }

  onCreateAccount(accountName: string, accountStatus: string) {
    this.accountsService.addAccount(accountName, accountStatus);
// this.loggingService.logStatusChange(accountStatus);
  }
}
```
