Introduction
============

This is used in very big applications. Where reducers receive message (as strings) which must be acted on by more complex actions (defined as classes).
So it replaces Services.
It is used to manage states, where a state is data where the application works with.
It implements the Facade pattern of the Gang of 4, with the dispatch function as the facade.

Actions - Reducers - Store
--------------------------
Reducer works always with data in and data out, which always a copy. Never use references. The reducer manages the app store.

For the actions we define unique identifiers and classes which optionally hold the payloads. Each class object has a type property which holds the identifier and a constructor which initializes the payload into a property.

For the identifiers use the pattern '[Class Name] Action to perform'

NgEffects
---------

NgRx is all about keeping the state uodated and clean. All other actions like storing the state in localstorage, http requests, error handling, redirecting, etc ... are Side effects, where we use NgEffects.

Here we use Actions class from Ngrx/effects (not store), which is a big observable.

The effects are side effects you attach to the states using 'pipe' and 'ofType'.

Router Store of NGRX
--------------------

Perform actions based on the router of Angular.

References
----------

Official Docs: https://ngrx.io
