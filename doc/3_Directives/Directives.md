Directives
==========

Directives are instruction in the DOM. Components are directives. Directives can be attributes to elements to modify the elements.

ngIf and else
-------------

ts-file:
```
export class TextComponent {
	visible : boolean = false;
...	
	showText() {
		return visible;
	}
}
```

html-file:
```
<p *ngIf="showText()">Hello, World</p>
```
'\*' means it is a structural directive! The HTML-element is added or not.

For an Else-part we use ng-template to reference the else part.

html-file
```
<p *ngIf="showText(); else noHello">Hello, World</p>
<ng-template #noHello>
  <p>Hi, World</p>
</ng-template>
```

ngStyle
-------

Attribute directives are like normal HTML attributes.

ts-file:
```
export class TextComponent {
	color : string = red;
...	
	getColor() {
		return color;
	}
}
```

html-file
```
<p [ngStyle]="{ backgroundColor: getColor() }">Some text</p>
```
[] is used to bind something to a property. ngStyle is the extended attribute from Angular.

ngClass
-------

Dynamically add or remove classes in elements.

ts-file:
```
export class TextComponent {
	textBig : boolean = true;
...	
	textBig() {
		return textBig;
	}
}
```

css-file
```
.big {
  font-size: large;
}
```

html-file
```
<p [ngClass]="{ big : textBig() }">Some text</p>
```

ngFor
-----

Show a Typescript array using ngFor to iterate of the array to display the content of the array.

ts-file:
```
export class TextComponent {
	lines = [ 'line 1', 'line2' ];
...	
}
```

html-file
```
<p *ngFor="let line of lines">{{line}}</p>
```

