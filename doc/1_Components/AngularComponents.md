Angular Components
==================

Command line
------------

Execute:
```
>ng generate component MyComponent
>ng g c MyComponent
```

Creates:
- src/app/my-component/my-component.component.html (HTML file)
- src/app/my-component/my-component.component.css (Style file)
- src/app/my-component/my-component.component.spec.ts (Typescript testing file)
- src/app/my-component/my-component.component.ts (Typescript with component logic)

and updates:
- src/app/app.module.ts:
```
import { SuccessAlertComponent } from './success-alert/success-alert.component';
import { WarningAlertComponent } from './warning-alert/warning-alert.component';
import { MyComponentComponent } from './my-component/my-component.component';

@NgModule({
  declarations: [
    AppComponent,
    SuccessAlertComponent,
    WarningAlertComponent,
    MyComponentComponent
  ],

```
It adds the line \"import { MyComponentComponent } from './my-component/my-component.component';\" to import the new component.
It adds the \"MyComponentComponent\" to the declaration list.

Manually
--------

You need to follow the naming convention as above for folders and files. You can also skip the html-file and css-file, if you integrate it into the ts-file itself

For example:

Create:
- src/app/my-component/my-component.component.ts
```
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-component',
  template: `
    <p>my-component works!</p>
  `,
  styles: [`
  	p { color: darkred; }
  `]
})
export class MyComponentComponent {

}
```

Update :
- src/app/app.module.ts

You need to add the line \"import { MyComponentComponent } from './my-component/my-component.component';\" to import the new component.
You need to add the \"MyComponentComponent\" to the declaration list.
