Observables
===========

An observable is a kind of data-source. It is based on the observable pattern, where you have an observer and an observable. The timeline can have events, which are emitted by the observable. The emit can by event, like button or http-response by the http-request. The observer, which is my code, listens to observable using the subscribe method.
There are 3 handlers you can listen to, handling the data, handling the errors, handling completion of the observable. This is the your code which is executed.
This how to handle asynchronous tasks without callbacks or promises.
The observable can emit one of the kind handlers during the passing of the time line.

rxjs compat
-----------
Used the old syntax for importing.

```
npm install --save rxjs-compat
```

Observables
-----------

Usage of the subscribe() method with a callback function, which is the observer. The subscribe() function takes 3 callbacks: observable data handling, observable error handling and a handler when the observable completes.
Check which handler makes sense in your app.

Implement an Observable
-----------------------

###interval

An even is emitted by the interval observable for every second.

Import interval from rxjs:

```
import {interval} from 'rxjs';
```

```
  ngOnInit() {
    const myNumber = interval(1000);
    myNumber.subscribe((number: number) => {
      console.log(number);
    });
  }
```

Every time you hit the home button, you will start the observable. You need to unsubscribe from it.
You store the return value of subscribe() into a subscription property and unsubscribe from it during onDestroy.

```
import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval, Subscription} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  private subscription: Subscription;

  constructor() { }


  ngOnInit() {
    this.subscription = interval(1000).subscribe( count => {
      console.log(count);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
```

Angular observables are managed by Angular and unsubscribe from them automatically.

### Oservable from scratch

To create an observable for you own business logic you use the create function and pass an anymous function with an Observer as a parameter. The Observer was the methods which you pass tos the subscribe function.

```
    const myObservable = Observable.create((observer: Observer<string>) => {
      setTimeout(() => {
        observer.next('First data package');
      }, 2000);
      setTimeout(() => {
        observer.next('Second data package');
      }, 4000);
      setTimeout(() => {
        observer.error('Error from observer');
  //      observer.complete();
      }, 5000);
      setTimeout(() => {
        observer.next('Third data package');
      }, 6000);
    });
```
When the error() or the complete() are used, the observable doesn't process the following functions anymore.

We use it and because the next passes a 'string', then the parameter of your anonymous function gets a string.
```
    myObservable.subscribe(
      (data: string) => {
        console.log(data);
      },
      (err: string) => {
        console.log(err);
      },
      () => {
          console.log('completed');
      }
      );
```

###Unsubscribe

As long as the observable has an active subscription, it will run your handlers. You need to unsubscribe your handlers from it. Even when you leave the component and the component with the observable is destroyed. So in the ngOnDestroy you need to unsubscrive the observable, which you created in the component.

```
export class HomeComponent implements OnInit, OnDestroy {

  numberObsSubscription: Subscription;
  myObsSubscription: Subscription;

  ngOnDestroy(): void {
    this.numberObsSubscription.unsubscribe();
    this.myObsSubscription.unsubscribe();
  }

  constructor() { }

  ngOnInit() {
    const myNumber = interval(1000);
    this.numberObsSubscription = myNumber.subscribe((number: number) => {
      console.log(number);
    });

    const myObservable = Observable.create((observer: Observer<string>) => {
      setTimeout(() => {
        observer.next('First data package');
      }, 2000);
      setTimeout(() => {
        observer.next('Second data package');
      }, 4000);
      setTimeout(() => {
        observer.error('Error from observer');
  //      observer.complete();
      }, 5000);
      setTimeout(() => {
        observer.next('Third data package');
      }, 6000);
    });

   this.myObsSubscription = myObservable.subscribe(
      (data: string) => {
        console.log(data);
      },
      (err: string) => {
        console.log(err);
      },
      () => {
          console.log('completed');
      }
      );
  }
}
```
During an error the observable dies.

Completing can be a normal process and fires the completed observer. The completed-observer is not called during an error.

Normally the cleanup of Angular observable do it themselves.

References:
- http://reactivex.io/

Operators
---------
Data  is passed by the observable to the observer. These data object thay can be passed through 'operators'.

Example: We will change the data using operators (like map from rxjs/operators) using pipes()
```
    this.subscription = customIntervalObservable.pipe(map( (data: number) => {
      return 'Round: ' + (data + 1);
    })).subscribe(
      data => {
        console.log(data);
      }, error => {
        alert(error);
      }, () => {
        console.log('completed');
      });
  }
```

Using 2 operators in the pipe:
```
    this.subscription = customIntervalObservable.pipe( filter( data => {
        return data > 0;
      }), map( (data: number) => {
      return 'Round: ' + (data + 1);
    })).subscribe(
      data => {
        console.log(data);
      }, error => {
        alert(error);
      }, () => {
        console.log('completed');
      });
```
Chain operators.

Subject
-------
Subject is like observable, but allows to push it to emit its data.
A Subject is an observable and observer at the same time, so you can call next() to it with the data you want to send to the observers.
You can build a Angular service using the Subject observable and send events to it.
You can call next from outside the observable.

Service:
```
import {Subject} from 'rxjs/Subject';

export class UsersService {
  userActivated = new Subject<number>();
}
```

Emit the button events
```
  constructor(private route: ActivatedRoute, private usersService: UsersService) { }
...
  onActivate() {
    this.usersService.userActivated.next(this.id);
  }
```

Listen to the service
```
  ngOnInit() {
    this.usersService.userActivated.subscribe((id: number) => {
      this.user1Activated = id === 1;
      this.user2Activated = id === 2;
    });
  }
```

Using Subject is preferrable to the complexities of EventEmitters.

You are obliged to unsubscribe from Subjects!

You can't use subject in combination with @Output, here you need the usage of event emitters.

###Check the RxJS documentation
Check all the operators you can use on the Observable.

Example of 'map' operator
```
 ngOnInit() {
    const myNumber = interval(1000).pipe(map((data: number) => data * 2 ));
...
}
```

Official Docs: https://rxjs-dev.firebaseapp.com/

RxJS Series: https://academind.com/learn/javascript/understanding-rxjs/

Updating to RxJS 6: https://academind.com/learn/javascript/rxjs-6-what-changed/