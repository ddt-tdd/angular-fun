Dynamic Components
==================

These are components, which are loaded on demand. You can communicate with them and afterwards destroy them.
These can be used for error, alert and warning dialogs.

Loading components
------------------

- Loading dynamic components dynamically using \*ngIf.
- Usage of dynamic component loader, we manually attach it into the DOM. You need to handle creation, setting of the paramters.

Usage of \*ngIf
---------------

To close the alert box. We need to emit an even

Manually create a component
---------------------------

Used in special situations.
To create components, it is not enough to just create the TypeScript object. You have to let Angular create the component using the ComponentFactoryResolver. This must be injected in the component, where you want to create the component.
The ComponentFactoryResolver will resolve your component and create a ComponentFactory, which you use to create the component and attach it into the DOM.

To attach it into the DOM, you need a ViewContainerRef using a helper directive. 
```
import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appPlaceholder]'
})
export class PlaceholderDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
```

This will hold the ViewContainerRef and can be used to point where the dynamic component will be place with the  ComponentFactory.
```
<ng-template appPlaceholder></ng-template>
<div class="row">
  <div class="col-xs-12 col-md-6 col-md-offset-3">
    <!--    <div class="alert alert-danger" *ngIf="error">
      <p>{{ error }}</p>
    </div> -->
    <!-- <app-alert [message]="error" *ngIf="error" (close)="onHandleError()"></app-alert> -->
    <div *ngIf="isLoading" style="position:absolute;top:33%;left:50%;">
      <app-loading-spinner></app-loading-spinner>
    </div>
...
```
We use not a div, but a ng-template as anchor.
We can access the ng-template using ViewChild, which will searcht the DOM using the class of the directive (PlaceholderDirective).

```
export class AuthComponent {
  isLoginMode = true;
  isLoading = false;
  error: string = null;
  @ViewChild(PlaceholderDirective, {static: false}) alertHost: PlaceholderDirective;
  
  constructor(private authService: AuthService,
              private router: Router,
              private componentFactoryResovler: ComponentFactoryResolver) {}
...
```

Now we can access the ViewContainerRef from the PlaceholderDirective to let the factory place the component. You ask the VieContainerRef to use the factory to place the component.
```
  private showErrorAlert(message: string) {
    // const alertCmp = new AlertComponent();
    const alertComponentFactory = this.componentFactoryResovler.resolveComponentFactory(AlertComponent);
    const hostViewContainerRef = this.alertHost.viewContainerRef;
    hostViewContainerRef.clear(); // remove previous visualizations
    hostViewContainerRef.createComponent(alertComponentFactory);
  }
```
You need to prepare Angular to create this component using an special property in the NgModule called 'entryComponents'. There components are allowed to be created dynamically.

app.module.ts
```
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RecipesComponent,
    RecipeListComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    ShoppingListComponent,
    ShoppingEditComponent,
    DropdownDirective,
    RecipeStartComponent,
    RecipeEditComponent,
    AuthComponent,
    LoadingSpinnerComponent,
    AlertComponent,
    PlaceholderDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [ShoppingListService, RecipeService, {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true }],
  bootstrap: [AppComponent],
  entryComponents: [AlertComponent]
})
export class AppModule { }
```

We need to manually bind the data and the events.

```
...
  ngOnDestroy(): void {
    if (this.closeSub) {
      this.closeSub.unsubscribe();
    }
  }

  private showErrorAlert(message: string) {
    // const alertCmp = new AlertComponent();
    const alertComponentFactory = this.componentFactoryResovler.resolveComponentFactory(AlertComponent);
    const hostViewContainerRef = this.alertHost.viewContainerRef;
    hostViewContainerRef.clear(); // remove previous visualizations
    const componentRef = hostViewContainerRef.createComponent(alertComponentFactory);
    componentRef.instance.message = message;
    this.closeSub = componentRef.instance.close.subscribe(() => {
      this.closeSub.unsubscribe();
      hostViewContainerRef.clear();
    });
  }
...
```

Refereneces
-----------

- https://angular.io/guide/dynamic-component-loader
