Property & Event binding
========================

Property binding and Event binding on:
- HTLM Elements -> Native properties and events
- Directives -> Custom Properties and events
- Components -> Custom Properties and events


@Input()
--------

Binding to our own properties.


```
      <app-server-element
        *ngFor="let serverElement of serverElements"
        [element]="serverElement">
      </app-server-element>
```

the [element] property is bound to the element property of the ServerElementComponent class. This is thanks to the @Input() decorator in front of the element property of the class

```
export class ServerElementComponent implements OnInit {
  @Input() element: {type: string, name: string, content: string};

  constructor() { }
...
}
```

To use an alias instead of element use 'srvElement' using an argument to @Input('srvElement')

@Output and EventEmitter
------------------------

Have a child component to send information to the parent component. We want to listen to events.

We add two custom event handles to listen to.
```
  <app-cockpit
    (serverCreated)="onServerAdded($event)"
    (bluePrintCreated)="onBluePrintAdded($event)"
  ></app-cockpit>
```

To signal the events from <app-cockpit> or the Cockpit-class, we add properties which are event emitters. When when click the button, we emit the information to the properties.

```
export class CockpitComponent implements OnInit {

  @Ouput() serverCreated = new EventEmitter<{serverName: string, serverContent: string}>();
  @Ouput() bluePrintCreated = new EventEmitter<{serverName: string, serverContent: string}>();

...

  onAddServer() {
    this.serverCreated.emit({serverName: this.newServerName, serverContent: this.newServerContent});
  }
```

@Output also support aliases.

```
  @Ouput('bpCreated') bluePrintCreated = new EventEmitter<{serverName: string, serverContent: string}>();
```

Note: When the distance of passing data between the component get too big, we should start thinking of using 'Services'

CSS behaviour in Angular
------------------------

Angular scopes the CSS behaviour to the component only. So no inheritance. It enforces style encapsulation using the an unique attribute names added to the styles elements. \_ngcontent-ejo-... it uses a shadow DOM.
-> View encapsulation

You can overwrite the encapsulation:

```
@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
  encapsulation: ViewEncapsulation.Emulated // Native, None, ShadowDOM
})
```
When set to 'Native' or 'None', the encapsulation is deactivated.
When set to 'ShadowDOM', the encapsulation is used in the ShadowDOM.

Local References
----------------
References will hold a reference to the whole element.
They uased as follows, using a '#' in front of serverNameInput:

```
    <input type="text" class="form-control" #ServerNameInput>
```

In the html they can be passed to events, using only the name:

```
    <button
      class="btn btn-primary"
      (click)="onAddServer(serverNameInput)">Add Server</button>
```

In the TypeScript code, we use it as the element and here it is the HTMLInputElement

```
  onAddServer(nameInput: HTMLInputElement) {
    this.serverCreated.emit({serverName: nameInput.value, serverContent: this.newServerContent});
  }
```

@ViewChild() to access the DOM
------------------------------

You cna directly access the DOM element to readout it, but don't use to set values.
You again use a reference in the HTML element.


```
<input type="text" class="form-control" #serverContentInput>
```

To access it you add a property to the class using the @ViewChild to acces the DOM element.

```
@ViewChild('serverContentInput', {static: true}) serverContentInput: ElementRef;
```

To access the data of the element.
```
  onAddServer(nameInput: HTMLInputElement) {
    this.serverCreated.emit({serverName: nameInput.value, serverContent: this.serverContentInput.nativeElement.value});
  }
```

<\ng-content> to project content
-------------------------------

You want to project the content between the Component's element tags into the element component HTML, then we use <\ng-content>.

In the component's element tag:

```
<app-server-element
  *ngFor="let serverElement of serverElements"
  [srvElement]="serverElement">
  <p>
    <strong *ngIf="serverElement.type === 'server'" style="color: red">{{ serverElement.content }}</strong>
    <em *ngIf="serverElement.type === 'blueprint'">{{ serverElement.content }}</em>
  </p>
</app-server-element>
```

Project it into your component app-server-element

```
<div
  class="panel panel-default">
  <div class="panel-heading">{{ element.name }}</div>
  <div class="panel-body">
    <ng-content></ng-content>
  </div>
</div>
```

Lifecycle
---------

Component lifecycle and events

1) ngOnChanges : when some input property is changed
2) ngOnInit: initialization, but not shown yet, object is created
3) ngDoCheck: Change detection runs, when something change is detected in the template
4) ngAfterContentInit: View of parent compenent is done and the ng-content is project and initialized, not the view of the component itself.You can access the content here using @ContentChild()
5) ngAfterContentChecked: This is called ng-content is checked
6) ngAfterViewInit: the component's view has been initialized together with the child views. So you can access the element content using @viewChild()
7) ngAfterViewChecked: the compenonet's view is checked on changes
8) ngOnDestroy: called when component's is destroyed -> cleanup of the object

