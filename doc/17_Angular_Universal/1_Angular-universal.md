Angular Universal
=================

This is used to pre-render the Angular App on the server before displaying it to the user.
It is still an SPA, but pre-rendered on the server.

References
----------

- https://angular.io/guide/universal
