import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value: any, propName: string): any {

    if (value.length > 0 && value[0][propName]) {
      const newValue = value.slice();
      newValue.sort((el1, el2) => {
        console.log(el1[propName] + '-' + el2[propName]);
        if (el1[propName] > el2[propName]) {
          return 1;
        }
        if (el1[propName] < el2[propName]) {
          return -1;
        }
        return 0;
      });
      return newValue;
    }
    return value;
  }

}
