import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {RecipeService} from '../recipes/recipe.service';
import {Recipe} from '../recipes/recipe.model';
import {exhaustMap, map, take, tap} from 'rxjs/operators';
import {AuthService} from '../auth/auth.service';

@Injectable({providedIn: 'root'})
export class DataStorageService {

  constructor(private httpClient: HttpClient,
              private recipeService: RecipeService,
              private authService: AuthService) {}

  storeRecipes() {
    const recipes = this.recipeService.getRecipes();
    this.httpClient.put('https://ng-recipe-book-01-695ab.firebaseio.com/recipes.json',
      recipes).subscribe((repsonseData) => {
        console.log(repsonseData);
      });
  }

  fetchRecipes() {
    return this.httpClient.get<Recipe[]>(
          'https://ng-recipe-book-01-695ab.firebaseio.com/recipes.json'
        ).pipe(
          map(recipes => {
            return recipes.map( recipe => {
              return { ...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
            });
          }),
          tap(recipes => {
            this.recipeService.setRecipes(recipes);
          })
        );
  }
}
