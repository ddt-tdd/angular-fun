import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';
import {AuthService} from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadedFeature = 'recipe';

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    firebase.initializeApp({
      apiKey: 'AIzaSyDx0J0SbUm6KjroOU-MwU3E6ezhVayb020',
      authDomain: 'ng-recipe-book-b2236.firebaseapp.com'
    });
    this.authService.initializeAuthService();
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
