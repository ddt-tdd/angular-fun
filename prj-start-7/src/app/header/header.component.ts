import {Component, OnDestroy} from '@angular/core';
import {RecipeService} from '../recipes/recipe.service';
import { Response} from '@angular/http';
import {DataStorageService} from '../shared/data-storage.service';
import {Subscription} from 'rxjs';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnDestroy {
  subSave : Subscription;
  subFetch : Subscription;

  constructor(private dataStorageService: DataStorageService,
              private recipeService: RecipeService,
              private authService: AuthService) {}

  onSave() {
    this.subSave = this.dataStorageService.storeRecipes().subscribe(
      (response: Response) => console.log(response),
      (error) => console.log(error)
    );
  }

  onFetch() {
    this.subFetch = this.dataStorageService.getRecipes().subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    );
  }

  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy(): void {
    if (this.subSave) {
      this.subSave.unsubscribe();
    }
    if (this.subFetch) {
      this.subFetch.unsubscribe();
    }
  }
}
