import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {RecipeService} from '../recipes/recipe.service';
import {catchError, map} from 'rxjs/operators';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class DataStorageService {
  constructor(private http: Http,
              private recipeService: RecipeService,
              private authService: AuthService) {}

  storeRecipes() {
    const token = this.authService.getToken();
    return this.http.put('https://ng-recipe-book-b2236.firebaseio.com/recipes.json?auth=' + token, this.recipeService.getRecipes());
  }

  getRecipes() {
    const token = this.authService.getToken();
    return this.http.get('https://ng-recipe-book-b2236.firebaseio.com/recipes.json?auth=' + token)
      .pipe(map(
        (response) => {
          const recipes = response.json();
          for (let recipe of recipes) {
            if (!recipe['ingredients']) {
              recipe['ingredients'] = [];
            }
          }
          this.recipeService.setRecipes(recipes);
          return 'Success Full';
        }
      ))
      .pipe(catchError(
        (error) => {
          console.log(error);
          return 'Something went wrong';
        }
      ));
  }

}
