import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  stopButtonDisabled = true;

  intervalHandle = 0;

  counter = 0;

  someText = 'Nothing';

  @Output() eventGame = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onStartCounter() {
    this.intervalHandle = setInterval(() => {
      console.log(this.counter);
      this.eventGame.emit(this.counter++);
    }, 1000);
    this.stopButtonDisabled = false;
  }

  onStopCounter() {
    clearInterval(this.intervalHandle);
    this.stopButtonDisabled = true;
  }

}
